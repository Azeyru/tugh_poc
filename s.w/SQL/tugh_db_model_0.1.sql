CREATE DATABASE tugh_dev;

CREATE TABLE `tugh_dev`.`genre` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `genre_name` VARCHAR(45) NULL,
  `genre_description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `tugh_dev`.`games` (
  `id` INT NOT NULL,
  `genre_id` VARCHAR(45) NULL,
  `game_name` VARCHAR(45) NOT NULL,
  `game_description` LONGTEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `tugh_dev`.`user_profile` (
  `id` INT NOT NULL,
  `user_name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(30) NOT NULL,
  `gaming_start-on` VARCHAR(45) NULL,
  `pc_spec` VARCHAR(255) NULL,
  `created_on` DATETIME NOT NULL,
  `last_seen` DATETIME NULL,
  `favourite_game` VARCHAR(255) NULL,
  `currently_playing` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `tugh_dev`.`user_profile` 
ADD COLUMN `streaming_time` DATETIME NULL AFTER `currently_playing`,
ADD COLUMN `streaming_platforms` VARCHAR(45) NULL AFTER `streaming_time`,
ADD COLUMN `todays_stream` INT NULL AFTER `streaming_platforms`;

CREATE TABLE `tugh_dev`.`user_games` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `game_id` VARCHAR(45) NULL,
  PRIMARY KEY (`user_id`));
