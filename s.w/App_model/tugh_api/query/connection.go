package query

import (
	"../configs"
	"database/sql"
	"fmt"
	"log"
	_ "github.com/go-sql-driver/mysql"
)

//Initiates connection to DB
func InitilizeDB() *sql.DB {
	log.Println("Connecting to DB...")
	var err error
	var connection *sql.DB

	config := configs.GetMainConfig()
	remote := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=UTC",
		config.GetDBUser(),
		config.GetDBPass(),
		config.GetDBIp(),
		config.GetDBPort(),
		config.GetSchemas())

	if connection, err = sql.Open(config.GetDialect(), remote); err != nil {
		fmt.Println("Failed Connecting to Database")
		panic(err.Error())
	}

	fmt.Printf("Connected to %s database[%s]\n", config.GetDialect(), config.GetSchemas())

	return connection
}
