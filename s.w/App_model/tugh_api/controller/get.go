package controller

import (
	"../repository"
	"../utils"
	"log"
	"net/http"
	"strconv"
)

func APIGetHomePage(w http.ResponseWriter, r *http.Request) {
    log.Println("HomePage Called")
    utils.JsonResponse("Welcome to TUGH Noobs", w)
}

func APIGetGenre(w http.ResponseWriter, r *http.Request) {
	ID := r.FormValue("ID")

	log.Println("API Get Genre Loaded")

	idd, _ := strconv.Atoi(ID)
	genreData := repository.GenreTableRepo(idd)

	w.WriteHeader(http.StatusOK)
	utils.JsonResponse(genreData, w)

}

func APIGetUserProfile(w http.ResponseWriter, r *http.Request) {
	ID := r.FormValue("ID")

	log.Println("Loading User Profile for" + ID)

	idd, _ := strconv.Atoi(ID)
	usrProfData := repository.UserProfileSelect(idd)

	w.WriteHeader(http.StatusOK)
	utils.JsonResponse(usrProfData, w)

}