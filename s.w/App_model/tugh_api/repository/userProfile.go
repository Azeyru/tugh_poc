package repository

import (
	"log"
	"../configs"
	"../query"
	"fmt"
)

type userProfileFrame struct {
	Id int
	Usr_name string
	Display_name string
	Gaming_start_on string
	Pc_spec string
	Created_on string
	Last_seen string
	Favourite_game string
	Currently_playing string
	Streaming_time string
	Streaming_Platforms string
	Todays_stream string
}

func UserProfileSelect(id int) userProfileFrame {
	mainConf := configs.GetMainConfig()
	usrProfConf := configs.GetUserConf()

	connection := query.InitilizeDB()
	defer connection.Close()

	log.Println("Selecting Data from User Profile")
	selectusrProfQuery := "SELECT * FROM %s WHERE %s=%d;"
	selectusrProfInf := []interface{}{
		mainConf.GetUsrProfile(),
		usrProfConf.GetUserProfID(),
		id}

	fmt.Printf(selectusrProfQuery, selectusrProfInf...)
	usrProfileInfo, err := connection.Query(fmt.Sprintf(selectusrProfQuery, selectusrProfInf...))
	if err != nil {
		log.Println("Error Fetching data from DB")
	}

	var usrProfData userProfileFrame

	for usrProfileInfo.Next(){
		if err := usrProfileInfo.Scan(&usrProfData.Id, &usrProfData.Usr_name, &usrProfData.Display_name, &usrProfData.Gaming_start_on, &usrProfData.Pc_spec, &usrProfData.Created_on, &usrProfData.Last_seen, &usrProfData.Favourite_game, &usrProfData.Currently_playing, &usrProfData.Streaming_time, &usrProfData.Streaming_Platforms,&usrProfData.Todays_stream); err == nil {
		} else {
			log.Println("Error While traversing data from DB")
		}
	}
	usrProfileInfo.Close()
	return usrProfData
}