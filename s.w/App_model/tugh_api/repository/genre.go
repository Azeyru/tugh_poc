package repository

import (
	"log"
	"../configs"
	"../query"
	"fmt"
)

type genreTableFrame struct {
	Id int
	Genre_name string
	Genre_description string
}

func GenreTableRepo(id int) genreTableFrame{

	mainConf := configs.GetMainConfig()
	genreConf := configs.GetGenreConfig()

	connection := query.InitilizeDB()
	defer connection.Close()

	log.Println("Selecting Data from Genre")
	selectGenreQuery := "SELECT * FROM %s WHERE %s=%d;"
	selectGenreInf := []interface{}{
		mainConf.GetGenreTable(),
		genreConf.GetGenreID(),
		id}

	genreInfo, err := connection.Query(fmt.Sprintf(selectGenreQuery, selectGenreInf...))
	if err != nil {
		log.Println("Error Fetching data from DB")
	}

	var genreData genreTableFrame

	for genreInfo.Next(){
		if err := genreInfo.Scan(&genreData.Id, &genreData.Genre_name, &genreData.Genre_description); err == nil {
		} else {
			log.Println("Error While traversing data from DB")
		}
	}
	genreInfo.Close()

	return genreData
}
/*
func main(){
	id := 1
	var dat genreTableFrame
	dat = genreTableRepo(id)
	log.Println("Data From DB")
	log.Println(dat)
}
*/