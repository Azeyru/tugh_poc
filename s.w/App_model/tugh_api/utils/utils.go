package utils

import (
	//"../configs"
	"encoding/json"
	"log"
	"net/http"
)

func JsonResponse(response interface{}, w http.ResponseWriter) {
	log.Println("Converting to JSON")
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
}

func ErrorResponse(err error, w http.ResponseWriter) {
	log.Println(err)
	w.WriteHeader(http.StatusInternalServerError)
	http.Error(w, "", 501)
	JsonResponse("Failed Loading, Data not found", w)
}
