package main

import (
	"log"
	"./routers"
)

func main() {

	log.Println("Initiating the TUGH API...")
	routers.HandleRequests()

}
