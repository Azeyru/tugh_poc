package configs

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type usrProfConf struct {
	TUGH struct {
		USERPROFILE struct {
			ID                string `yaml:"ID"`
			USRNAME           string `yaml:"USRNAME"`
			DISPLAYNAME       string `yaml:"DISPLAYNAME"`
			GAMINGSTART       string `yaml:"GAMINGSTART"`
			PCSPEC            string `yaml:"PCSPEC"`
			CREATEDON         string `yaml:"CREATEDON"`
			LASTSEEN          string `yaml:"LASTSEEN"`
			FAVGAME           string `yaml:"FAVGAME"`
			CURRENTLYPLAYING  string `yaml:"CURRENTLYPLAYING"`
			STREAMINGTIME     string `yaml:"STREAMINGTIME"`
			STREAMINGPLATFORM string `yaml:"STREAMINGPLATFORM"`
			TODATSSTREAM      string `yaml:"TODATSSTREAM"`
		}`yaml:"USERPROFILE"`
	}`yaml:"TUGH"`
}

func GetUserConf() usrProfConf {
	log.Println("Reading User Profile Config")
	yamlData, err := ioutil.ReadFile("./configs/tableConfig.yaml")
	if err != nil {
		log.Fatal("Error loading table config yaml for user profile, kindly check 'tableConfig.yaml'" + err.Error())
	}

	var profconf usrProfConf
	err = yaml.Unmarshal(yamlData, &profconf)
	if err != nil {
		log.Fatal("Error loading user profiles info, kindly check user profile datatype" + err.Error())
	}
	log.Println("Success: Reading User Profile Config")
	return profconf
}

func (c* usrProfConf) GetUserProfID() string{
	return c.TUGH.USERPROFILE.ID
}

func (c* usrProfConf) GetUsrName() string{
	return c.TUGH.USERPROFILE.USRNAME
}

func (c* usrProfConf) GetDisplayName() string{
	return c.TUGH.USERPROFILE.DISPLAYNAME
}

func (c* usrProfConf) GetGamingStat() string{
	return c.TUGH.USERPROFILE.GAMINGSTART
}

func (c* usrProfConf) GetPcSpec() string{
	return c.TUGH.USERPROFILE.PCSPEC
}

func (c* usrProfConf) GetCreatedOn() string{
	return c.TUGH.USERPROFILE.CREATEDON
}

func (c* usrProfConf) GetLastSeen() string{
	return c.TUGH.USERPROFILE.LASTSEEN
}

func (c* usrProfConf) GetFavGames() string{
	return c.TUGH.USERPROFILE.FAVGAME
}

func (c* usrProfConf) GetCurrentlyPlaying() string{
	return c.TUGH.USERPROFILE.CURRENTLYPLAYING
}

func (c* usrProfConf) GetStreamTime() string{
	return c.TUGH.USERPROFILE.STREAMINGTIME
}

func (c* usrProfConf) GetStreamPlat() string{
	return c.TUGH.USERPROFILE.STREAMINGPLATFORM
}

func (c* usrProfConf) GetTodayStream() string{
	return c.TUGH.USERPROFILE.TODATSSTREAM
}