package configs

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type genreConf struct {
	TUGH struct {
		GENRE struct {
			ID         string `yaml:"ID"`
			GENRENAME  string `yaml:"GENRENAME"`
			GENREDESC  string `yaml:"GENREDESC"`
		} `yaml:"GENRE"`
	} `yaml:"TUGH"`
}

func GetGenreConfig() genreConf {
	log.Println("Reading Genre Config")
	yamlFile, err := ioutil.ReadFile("./configs/tableConfig.yaml")
	if err != nil {
		log.Fatal("Error loading table config yaml, kindly check 'tableConfig.yaml'" + err.Error())
	}

	var genrecon genreConf
	err = yaml.Unmarshal(yamlFile, &genrecon)

	if err != nil {
		log.Fatal("Error loading genre info, kindly check genre datatype" + err.Error())
	}
	log.Println("Success: Reading Genre Config")
	return genrecon
}

func (c* genreConf) GetGenreID() string{
	return c.TUGH.GENRE.ID
}

func (c* genreConf) GetGenreName() string{
	return c.TUGH.GENRE.GENRENAME
}

func (c* genreConf) GetGenreDesc() string{
	return c.TUGH.GENRE.GENREDESC
}
