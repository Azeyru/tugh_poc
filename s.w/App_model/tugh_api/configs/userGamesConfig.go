package configs

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type userGames struct {
	TUGH struct {
		USERGAMES struct {
			USERID string `yaml:"USERID"`
			GAMEID string `yaml:"GAMEID"`
		}`yaml:"USERGAMES"`
	}`yaml:"TUGH"`
}

func GetUsrGamesConfig() userGames {
	log.Println("Reading User Games Config")
	yamlFile, err := ioutil.ReadFile("./configs/tableConfig.yaml")
	if err != nil {
		log.Fatal("Error loading table config yaml, kindly check 'tableConfig.yaml'" + err.Error())
	}

	var usrgamescon userGames
	err = yaml.Unmarshal(yamlFile, &usrgamescon)
	if err != nil {
		log.Fatal("Error loading genre info, kindly check genre datatype" + err.Error())
	}
	log.Println("Success: Reading User Games Config")
	return usrgamescon
}

func (c* userGames) GetUsersID() string{
	return c.TUGH.USERGAMES.USERID
}

func (c* userGames) GetGameID() string{
	return c.TUGH.USERGAMES.GAMEID
}