package configs

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type mainConfig struct {
	TUGH struct {
		MySQL struct {
			DB_DIALECT string `yaml:"DB_DIALECT"`
			DB_USER    string `yaml:"DB_USER"`
			DB_PASS    string `yaml:"DB_PASS"`
			DB_IP      string `yaml:"DB_IP"`
			DB_PORT    string `yaml:"DB_PORT"`
			SCHEMAS    string `yaml:"SCHEMAS"`
		} `yaml:"MySQL"`
		TABLE struct {
			GENRETABLE   string `yaml:"GENRETABLE"`
			GAMESTABLE   string `yaml:"GAMESTABLE"`
			USERGAME     string `yaml:"USERGAME"`
			USERPROFILE  string `yaml:"USERPROFILE"`
		} `yaml:"TABLE"`
		APP struct {
			PORT string `yaml:"PORT"`
		} `yaml:"APP"`
	} `yaml:"TUGH"`
}

func GetMainConfig() mainConfig {
	log.Println("Reading Main Config")
	yamlFile, err := ioutil.ReadFile("./configs/mainConfig.yaml")
	if err != nil {
		log.Println("Error Reading YAML file" + err.Error())
	}

	var conf mainConfig
	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil {
		log.Println("Error Parsing YAML file" + err.Error())
	}
	log.Println("Success: Reading Main Config")
	return conf
}

//DB Info
func (c *mainConfig) GetDialect() string {
	return c.TUGH.MySQL.DB_DIALECT
}

func (c *mainConfig) GetDBUser() string {
	return c.TUGH.MySQL.DB_USER
}

func (c *mainConfig) GetDBPass() string {
	return c.TUGH.MySQL.DB_PASS
}

func (c *mainConfig) GetDBIp() string {
	return c.TUGH.MySQL.DB_IP
}

func (c *mainConfig) GetDBPort() string {
	return c.TUGH.MySQL.DB_PORT
}

func (c *mainConfig) GetSchemas() string {
	return c.TUGH.MySQL.SCHEMAS
}

//Main Table info
func (c *mainConfig) GetGenreTable() string {
	return c.TUGH.TABLE.GENRETABLE
}

func (c *mainConfig) GetGamesTable() string {
	return c.TUGH.TABLE.GAMESTABLE
}

func (c *mainConfig) GetUsrGame() string {
	return c.TUGH.TABLE.USERGAME
}

func (c *mainConfig) GetUsrProfile() string {
	return c.TUGH.TABLE.USERPROFILE
}

//APP config
func (c *mainConfig) GetAppPort() string {
	return c.TUGH.APP.PORT
}