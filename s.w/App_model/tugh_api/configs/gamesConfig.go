package configs

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type gamesConf struct {
	TUGH struct {
		GAMES struct {
			ID        string `yaml:"ID"`
			GENREID   string `yaml:"GENREID"`
			GENRENAME string `yaml:"GENRENAME"`
			GAMEDESC  string `yaml:"GAMEDESC"`
		}`yaml:"GAMES"`
	}`yaml:"TUGH"`
}

func GetGamesConf() gamesConf {
	log.Println("Reading Games Config")
	yamlData, err := ioutil.ReadFile("./configs/tableConfig.yaml")
	if err != nil {
		log.Fatal("Error loading table config yaml for games, kindly check 'tableConfig.yaml'" + err.Error())
	}

	var gamescon gamesConf
	err = yaml.Unmarshal(yamlData, &gamescon)
	if err != nil {
		log.Fatal("Error loading games info, kindly check games datatype" + err.Error())
	}
	log.Println("Success: Reading Games Config")
	return gamescon
}

func (c* gamesConf) GetGamesID() string{
	return c.TUGH.GAMES.ID
}

func (c* gamesConf) GetGenreID() string{
	return c.TUGH.GAMES.GENREID
}

func (c* gamesConf) GetGenreName() string{
	return c.TUGH.GAMES.GENRENAME
}

func (c* gamesConf) GetGenreDesc() string{
	return c.TUGH.GAMES.GAMEDESC
}
