package routers

import (
	"../configs"
	"../controller"
	//"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func HandleRequests() {
    /*
    allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With", "Authorization"})
    allowedOrigins := handlers.AllowedOrigins([]string{"*"})
    allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "OPTIONS"})
    */
    mainRouters := mux.NewRouter().StrictSlash(true)
    mainRouters.HandleFunc("/home", controller.APIGetHomePage).Methods("GET")
    mainRouters.HandleFunc("/getGenre", controller.APIGetGenre).Methods("GET")
    mainRouters.HandleFunc("/getUserProfile", controller.APIGetUserProfile).Methods("GET")

    mainConf := configs.GetMainConfig()
    port := mainConf.GetAppPort()

    log.Println("Listening to " + mainConf.GetAppPort())
    err := http.ListenAndServe(port, mainRouters)
    //err := http.ListenAndServe(port, handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(mainRouters))
    if err != nil{
    	log.Fatal("Failed connecting to port " + port)
    }     
}
